package me.ihwan.example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText panjang, lebar, tinggi;
    private Button hitung;
    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panjang = findViewById(R.id.panjang);
        lebar = findViewById(R.id.lebar);
        tinggi = findViewById(R.id.tinggi);
        hitung = findViewById(R.id.hitung);
        hasil = findViewById(R.id.hasil);

        hitung.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if( v.getId() == R.id.hitung){

            String angka1 = panjang.getText().toString().trim();
            String angka2 = lebar.getText().toString().trim();
            String angka3 = tinggi.getText().toString().trim();

            boolean textKosong = false;

            if(TextUtils.isEmpty(angka1)){
                textKosong = true;
                panjang.setError("isi dulu bro panjangnya!");
            }
            if(TextUtils.isEmpty(angka2)){
                textKosong = true;
                lebar.setError("kamu lupa isi lebar!");
            }
            if(TextUtils.isEmpty(angka3)){
                textKosong = true;
                tinggi.setError("ini juga di isi bro!");
            }
            if (!textKosong) {
                double p = Double.parseDouble(angka1);
                double l = Double.parseDouble(angka2);
                double t = Double.parseDouble(angka3);

                double jumlah = p * l * t;

                hasil.setText(String.valueOf("Hasil " + jumlah));
            }

        }

    }
}
